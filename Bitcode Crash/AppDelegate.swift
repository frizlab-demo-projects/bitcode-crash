/*
 * AppDelegate.swift
 * Bitcode Crash
 *
 * Created by François Lamboley on 2021/2/20.
 */

import UIKit

import XibLoc



@main
class AppDelegate: UIResponder, UIApplicationDelegate {
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		/* Export options: no app thinning, bitcode and no swift symbol stripping. */
		NSLog("%@", "|bob| says hello".applyingCommonTokens(simpleReplacement1: "Frizlab"))
		return true
	}
	
}
